// Fill out your copyright notice in the Description page of Project Settings.


#include "BarrierElement.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Barrier.h"
#include "SnakeBase.h"

// Sets default values
ABarrierElement::ABarrierElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ABarrierElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABarrierElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABarrierElement::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake)) {
			Snake->Destroy();
			GEngine->AddOnScreenDebugMessage(1, 3.f, FColor::Blue, TEXT("GAME OVER!"));			
		}
	}
}

