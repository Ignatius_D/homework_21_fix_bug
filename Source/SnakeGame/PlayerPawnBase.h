// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent; //For don't use include header, true for next
class ASnakeBase; //use prefix A, because child from Actor


UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BluePrintReadWrite)//Next variable can read and write in Blueprints
	UCameraComponent* PawnCamera;//Ptr to camera

	UPROPERTY(BluePrintReadWrite)//Next variable can read and write in Blueprints
	ASnakeBase* SnakeActor;//Ptr to our actor of snake

	UPROPERTY(EditDefaultsOnly)//Next variable can change only in default settings
	TSubclassOf<ASnakeBase> SnakeActorClass;//Ptr to class (not object), for transit to spawn 

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);

	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);

};
